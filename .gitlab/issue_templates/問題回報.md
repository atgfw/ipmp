## 您所处的网络环境

- 是否在中国 GFW 内？

  - 如果是，问题发生在有翻墙时还是无翻墙时？
    （是否在另一种环境成功过？）


## 使用的瀏覽器

- 請說明使用的瀏覽器，包括在 PC 还是在手机，例如 Chrome, Firefox, Safari 等等：

## 錯誤的網址

- 請說明發生錯誤时使用的網址：

## 說明與截圖

- 网页页面的报错信息：

- HTTP 状态码：（可以按 F12）

- 除文字說明外，請盡量截圖，方便他人理解：

## 您期待的结果