import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { ColumnistLiner, ColumnistLarge } from '../../components/ColumnistTemplate'

const ColumnistPreview = ({ entry, getAsset }) => {
  const data = entry.getIn(['data']).toJS()
  const title = data.title || ''
  const headerStyle = {
    fontSize: '1.2rem',
    fontWeight: 'bold',
    margin: '1rem 0'
  }
  const wrapperStyle = {
    border: '1px solid #aaa',
    margin: '1rem',
    padding: '1rem',
    borderRadius: '0.25rem'
  }
  const [cover, setCover] = useState('')

  useEffect(() => {
    const newCover = entry.getIn(['data', 'cover'])
    if (newCover) {
      getAsset(newCover).then(realCover => {
        setCover(realCover.toString())
      })
    }
  })
  return (
    <div className="content">
      <h2 style={headerStyle}>于专栏列表使用的大图像</h2>
      <div style={wrapperStyle}>
        <ColumnistLarge
          slug="/#"
          title={title}
          cover={cover}
        />
      </div>
      <h2 style={headerStyle}>于个别专栏使用的小图像</h2>
      <div style={wrapperStyle}>
        <ColumnistLiner
          title={title}
          cover={cover}
        />
      </div>
    </div>
  )
}

ColumnistPreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  widgetFor: PropTypes.func,
  getAsset: PropTypes.func
}

export default ColumnistPreview
