import React from 'react'
import PropTypes from 'prop-types'
import { ArticleTemplate } from '../../templates/article'

const ArticlePreview = ({ entry, widgetFor }) => {
  const data = entry.getIn(['data']).toJS()
  const tags = data.tags || []
  const contributors = data.contributors || []
  const dayRaw = data.date || Date.now()
  // YYYY/MM/DD
  const day = [
    dayRaw.getFullYear(),
    dayRaw.getMonth().toString().padStart(2, 0),
    dayRaw.getDate().toString().padStart(2, 0)
  ].join('/')
  return (
    <ArticleTemplate
      content={widgetFor('body')}
      description={data.description || ''}
      tags={tags}
      title={data.title || ''}
      date={day}
      contributors={contributors}
      isbrief={data.isbrief || ''}
    />
  )
}

ArticlePreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  widgetFor: PropTypes.func,
}

export default ArticlePreview
