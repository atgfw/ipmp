---
templateKey: article
title: 2017，逃离不会有未来
slug: 2017-jan-05-article-no-future
date: 2017-01-05T09:36:57.635Z
description: 进步的背后是代价。冤案平反了，为之奔走的公民、律师备受打击；慈善相关法律通过了，个人不能募捐境外机构要面临撤离，公益人被抓被打击报复仍没有保障。
featuredpost: false
featuredimage: /img/黄鸭.jpg
trending: 3
isbrief: false
tags:
  - 逃离
  - 公义
  - 抗争
---
这是2017年的第一天，北方的雾霾依旧肆虐，一路直下，遍及南北。

伴随环境雾霾带来的是笼罩在人心上的雾霾，它更让人忧心不安。

我们并不想妖魔化过去这一年。这一年里，我们看到了民间推动的进步，乐平案聂树斌案陆续获得平反；我们看到公益慈善获得重视，<<慈善法>>、<<境外组织管理法>>等相继出台，让慈善有法可依；我们看到倡导并未远离，公益诉讼，影响力诉讼一波接一波，似乎民间仍然大有可为。

然而进步的背后是代价。冤案平反了，为之奔走的公民、律师备受打击；慈善相关法律通过了，个人不能募捐境外机构要面临撤离，公益人被抓被打击报复仍没有保障。

在更微小的细节里，讲述性别平等的课程被停课，为青年提供活动的空间被限制，为工友服务的公益机构被逼迁，从事社会发展工作的伙伴恐惧感不断加深.....这一切的一切，怎么能让我们欢欣鼓舞？

再把目光投向2016年的这片土地，当村民贾敬龙强拆杀人被判死刑，雷洋一案涉事警员却罪轻不被起诉，我们该如何相信公平会到来？当青年魏则西因虚假医疗广告而死，百度既不负责还整而不改，我们该如何相信这国的商业有底线存在？当毒地毒跑道层出不穷，公益机构屡屡起诉相关部门环评却永远合格，我们该如何相信孩子真被看成国家的未来？

这一年里，太多的故事戛然而止，我们都清楚发生了什么，道路以目，是这一年最好的形容词。

如果说，慈善公益是一门蓬勃发展的事业，叩心自问，在看到巨大社会价值的同时，我们该如何面对这些从底层到中产积累的愤怒？该如何面对一声声从个体到群体发出的悲鸣？在这个被撕扯，急剧分化坠落的社会里，眼睁睁的看着动荡加剧却蒙眼自闭说无能为力，若干年后，当孩子问起我们，在不公平不正义蔓延这片土地时，你们做过些什么，努力过什么，我们又该如何做答？

2017会更好吗？没有答案。我们常说，明天会更好，现实往往却是明天更残酷。

我们需要一种有担当的爱。这种爱，是直面当下的问题，这种爱，是明知一时不会有结果仍奋起而行之，这种爱，是明知面临不可测的风险，也站出来勇敢付诸行动。这种爱，是雷洋案中的校友会，一步步叩问真相，是律师、公民和他们的家属，为公义而呐喊，是困境中坚守的NGO，执着行动，力争改变。

告慰当下，抚平我们伤口的，不是悲观绝望的情绪，不是自欺欺人的逃避，勇敢而悲悯的行动才是最有力的武器。历史告诉我们，环境破坏带来的雾霾通过治理能够散去，拂去人心的雾霾，也需要世代的抗争才会有长久的未来。