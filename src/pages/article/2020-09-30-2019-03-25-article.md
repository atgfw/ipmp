---
templateKey: article
title: 爆炸四天后，园区内水质仍超标，环境影响需持续关注
slug: 2019-03-25-article
date: 2019-03-25T12:37:19.464Z
featuredpost: false
featuredimage: /img/shangshui.webp
trending: 1
isbrief: false
tags:
  - 环境
  - 社会热点
---
`最新监测结果显示，园区内新丰河闸内水质仍严重超标，其中氨氮浓度超标111倍，苯胺类超标63.7倍，二氯甲烷超标21倍。`

作者丨小田、扶雨、捞面

制图丨一舟

江苏盐城响水化工厂爆炸事故后，环境影响方面，水质问题最受关注。

事故发生地距离最近的灌河河道不足2公里，距离灌河入海口仅十几公里，如果污水流入灌河，则可能污染黄海。据了解，在事故次日上午，化工园区内的新民河、新丰河和新农河三条入灌河的河渠已经完成封堵。截至25日上午六时，由江苏省生态环境厅所测得的数据显示，灌河入海口、厂区排污口下游3公里2个点位均无出现超标情况。

但是，园区内的新丰河闸内水质一直超标。

这是22日至25日，新丰河水质监测情况：

![](https://img9.doubanio.com/view/note/l/public/p59240102.webp)

响水化工园新丰河闸内环境监测值变化图，数据来源：江苏省生态环境厅

监测数据显示，园区内新丰河闸内的氨氮、苯胺、二氯甲烷、二氯乙烷等均持续严重超标，甲苯的浓度则比第一天高出近三倍。另外，新丰河水中苯浓度为首次公布，亦显示超标一倍。

中山大学化学学院环境化学研究所所长欧阳钢锋告诉NGOCN，水中的苯不经过深度处理在自然环境中很难被降解，虽然苯在水中的溶解度低，但国家规定的标准值是很低的，需要持续进行监测，关注变化趋势。另外，由于甲苯在水中溶解度低，所以随着时间慢慢溶解，其在水中的浓度会上升。

此外，24日的新闻发布会上通报了，有化工企业存储的硫酸和硝酸泄漏，但并未公布具体泄漏数量和位置。

清华大学化学博士、科普工作者孙亚飞向NGOCN表示，现场的高浓度强酸有腐蚀性，进入水体可能会对地下管网造成损害，处理起来会更麻烦。不过，由于泄漏量不明，以及不清楚高浓度的硫酸和硝酸会不会在消防作业中已经被稀释，暂时还无法判断其危害程度。

爆炸后对土壤的影响也同样值得关注。欧阳钢锋指出，因为苯类物质在水和空气中，会因为水和空气的流动而浓度下降较快，而工厂附近的土壤会吸附这些物质后逐步释放，因此土壤需要在一段较长时间内持续进行监测。

目前，江苏省环境监测中心已经完成对现场废存积水和土壤的采样。

相关阅读：[响水爆炸48小时，仍有化工厂人员失联](https://mp.weixin.qq.com/s/oP9NjJ-8iVXHVMIFYplR2A)