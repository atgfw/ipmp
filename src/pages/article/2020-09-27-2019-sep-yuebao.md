---
templateKey: article
title: NGOCN丨九月事件防腐剂
slug: 2019-sep-yuebao
date: 2019-10-01T16:59:00.000Z
featuredpost: false
featuredimage: /img/连侬墙.webp
trending: 0
isbrief: false
tags:
  - 贵价猪，人脸识别，反送中，吹哨人
---
# NGOCN丨九月事件防腐剂

2019 年 10 月 2 日

**这是NGOCN“事件防腐剂”专栏：每月回顾，别让公共事件烂在角落里！**

**以下是2019年9月的公共事件回顾：**

**作者 | 顾家明**

**编辑丨阿八**



## 贵价猪



早餐店的肉包子贵了五毛到一块，这应该不是什么新鲜事了，大家都知道菜市场的猪肉贵了许多。据农业农村部新闻办消息，**9月的第三周（16-22日），猪肉价格已达到42.57元/公斤。**

![](https://assets.matters.news/embed/a8020cc1-83e6-4dd4-baea-988569e0f37d.webp)

图片来源21世纪经济报，作者：谢珍、张楠（生猪价格与猪肉价格为两个不同指标）

生猪稳产保供已经成了一项政治任务，国务院胡副总理在全国稳定生猪生产保障市场供应电视电话会议上做出了强调。自8月下旬，发改委、财政部等多个部门发布了与生猪产业相关的优惠政策。**新京报9月20日统计，相关部门至少14次出手稳定猪肉价格。**



其中值得注意的是，生态环境部与农业农村部排除禁养区划定搞“一刀切”，以确保生猪产量。土壤生态环境司司长苏克敬在记者会更直接提到，不能打着环保的旗号，借改善环境质量之名，随意扩大禁养区。



另外，国庆70周年来临前，国家“华商储备商品管理中心”在本月先后三次投放中央储备冻猪肉，目前已累计投放3万吨。而更早之前，不少地方政府就开始投放地方储备肉，推出平价猪肉。继福建两地推出限购平价猪肉后，广西南宁在九月也实施了类似措施，市民可购买到低于前10日市场均价10%以上的猪肉，但需持身份证购买，且限购1公斤。

![](https://assets.matters.news/embed/a2ee4823-28f5-4e79-91d6-b034e9124850.webp)

图片来源21世纪经济报，作者：谢珍、张楠（生猪价格与猪肉价格为两个不同指标）

最新数据显示，9月的第三周（16-22日），猪肉价格已经上涨趋缓，涨幅为1.6%。9月底，国务院胡副总理提出，生猪生产要确保明年元旦春节期间供应，**力争明年基本恢复到正常水平**。



## 人脸安全？

9月初，一款名为“ZAO”的App因可通过AI给用户换脸，制作趣味视频或表情包，上线不到24小时即冲上各大应用商店下载排行前列，爆红网络。然而，就在ZAO上线的第五天，ZAO就被工信部约谈，要求整改。

![](https://assets.matters.news/embed/f8106dcb-3759-4178-917d-5d030f94b599.webp)

图为“ZAO”App页面截图 图片来源网络

约谈的起源是，ZAO的隐私协议上写着“**对用户信息拥有完全免费、不可撤销、永久、可转授权和可再许可的权利”**内容引起了不少关于数据安全的担忧和批评。ZAO回应称不会存储用户人脸信息，目前隐私协议中相关内容已被删除。

“刷脸”的技术越来越发达，“人脸识别”也被广泛应用到各种场合中，其中就包括校园。相对学校和宿舍的人脸识别门禁，**教室里头的“人脸识别”监控无疑更刺激大众的敏感点**。

九月新学期开始，中国药科大学在两间教室试点安装了人脸识别系统，用于日常考勤和课堂纪律管理。针对侵犯隐私质疑，学校工作人员则回应不存在该问题，**因为教室属于公开场所**。

相对于高校，人脸识别应用于课堂更早出现在中小学。在上海8月底举办的世界人工智能大会中，上海闵行区一所小学就被当作是“AI+校园”的成功案例。据宣传资料，**学校去年开始就在课堂使用人脸识别技术，系统分析教师和学生行为以服务教学**。

![](https://assets.matters.news/embed/a9477565-f6a4-4039-9679-e3d0c070b17f.webp)

图为上海闵行区一小学AI监控系统 图片来源网络

但相关监控画面在网上曝光之后，引发不少讨论。9月2日，学校技术支持方“旷视科技”回应，称“教育领域的产品专注于保护孩子在校园的安全”。



社会对学校加装人脸识别显然是谨慎的。9月5日，教育部科学技术司司长雷朝滋亦向媒体表示：“希望学校非常慎重地使用这些技术软件”，“**学生的个人信息都要非常谨慎，能不采集就不采，能少采集就少采集。**”





## 九月香港



9月4日17时50分，香港特首林郑月娥通过电视讲话宣布：**正式撤回《逃犯条例》修订草案。**距离6月9日103万人上街“反修例”已经过去87日。

![](https://assets.matters.news/embed/3383a38a-d55b-42ff-ac80-f2bbdcafdab5.webp)

9月2日开学日，香港有大学生和中学生宣布罢课，要求政府回应诉求。图来自BBC CHINESE

林郑月娥在讲话中还提到，**将不设立专门的独立调查委员会**，而是继续由监警会进行处理，会邀请国际专家加入调查，“7·21元朗袭击事件”亦会作为调查的重点。调查报告的内容和建议都会公开。



另外一个重点则是，她将到社区与市民直接对话，“让社会各个阶层、不同政治立场、不同背景的人士，透过对话平台，将种种不满直接说出来，一起去探讨解决方法”。



9月26日，第一场社区对话在湾仔伊利沙伯体育馆举行，参加者包括从超过2万名报名者中抽签得出的150名市民。该场对话亦在脸书进行了全程直播。对话前，不少学校、商店提前关闭，大批警员则携带如胡椒喷雾等装备到现场戒备。最后，对话持续了约两个半小时。据香港01统计，现场发言的30人中，**25人“狠批林郑和警方”，5人“直接或间接批评激烈示威活动”**。

![](https://assets.matters.news/embed/1bc90e0d-20ef-4af3-a00e-a41ecdbfd9a9.webp)

图为9月26日社区对话现场，图来自GETTY IMAGES

第一场社区对话过后，各方意见不一。行政会议成员、资深大律师汤家骅表示“相当满意”，认为林郑月娥没有对成立独立调查委员会的诉求“关上大门”。新华社则评论称“行政长官林郑月娥以诚恳谦卑的态度，认真聆听每一位市民的意见”，认为谈话“开启抚平香港社会伤痕的契机”。



然而，多名参与者则向媒体表示，**对话会是“各说各话”**。属于建制派阵营的民建联副主席蒋丽芸批评，林郑月娥举行对话，却没有实质上回应市民诉求会“适得其反”；工党立法会议员张超雄则表示，林郑应该去解决问题，聆听却什么都不做“没意思”。



九月，**“连侬墙”亦成为“争夺”的焦点。**连侬墙，即把各种便签纸、海报等贴在墙上，上面有各种标语、字句或者图画，2014年开始出现在香港。在此次的运动中，“连侬墙”在香港各个区陆续出现，是不少示威者发表心声的场地。



![](https://assets.matters.news/embed/853899ba-3414-497e-9e38-480d9aa2b61c.webp)

一个位于荃湾的连侬墙，图自巴士的报

从出现开始，因为“撕毁”与“保护”而发生的冲突就不断出现。香港警方在20日的记者会上表示，6月至今已处理40起因连侬墙而发生的冲突，拘捕了57人。本月21日，立法会议员何君尧发起了“清洁香港运动”——号召市民在18个区对连侬墙进行清理。但当日，至少三个区都因此发生肢体冲突。港区政协委员林大辉因此批评，认为清洗连侬牆会衍生冲突，引发动荡，是火上加油之举。



尽管如此，9月30日，食环署职员开始清理各区连侬墙。据媒体报道，香港18个区的卫生部门都收到了食环署的命令，需要对连侬墙进行清理。



除此之外，集会与示威仍在继续，石块与纵火在继续，胡椒喷雾与催泪弹仍在继续，拘捕与检控亦在继续。



![](https://assets.matters.news/embed/f87d629f-6a01-4713-beb8-bd1f45f0a6f8.webp)

香港政府9月18日宣布取消了今年的国庆烟花汇演。（图为timable网页截图）



## 下架



香港之外，不少事情都与正在发生的“风波”有关。



**大陆，大班月饼在电商平台及线下超市被全线下架。**起因则是香港总公司的“太子爷”郭勇维在脸书上分享“人链”活动信息，被指支持“乱港分子”。《人民日报》发评论称“谁非要在月饼里塞私货，公然支持乱港分子，那么激起公愤、被拒之门外是就是理所当然的”。



只是，作为经销商的永桦盛商贸有限公司则略显无奈。其在事件发生后在大班月饼官方微博发布声明，称郭勇维的言论“不代表本公司立场”，亦附上郭的道歉声明，可惜于事无补。作为永桦盛线上销售负责人的吴浩田向《南方都市报》表示，所有渠道的月饼都将退回到永桦盛，退回的月饼无法运回香港，只能进行销毁处理。



香港组合My Little Airport亦遭到了全网下架的对待。下架前一天，**微博博主“孤烟暮蝉”发文指责该组合为“深黄”（意为香港极端反对派），不少留言均表示要进行举报。**乐队成员之一的林阿P随后在微博公布了自己的Instagram帐号，之后未再发布微博消息。



![](https://assets.matters.news/embed/5276e1dd-9c9e-47b9-a445-ebb87da3db7d.webp)

该组合新歌《吴小姐》MV截图

该组合成立于2003年，成员为林阿P与Nicole二人，作品关注香港社会、青年、本土议题等。在此次的反修例事件中，亦有新歌表示对反修例人士的支持，今年十一月的演唱会更命名为**“the taste of tears 催泪的滋味”。**



除了全线下架，也有比较特殊的情况——台湾作家龙应台的作品在京东一家平台被下架。龙应台9月初在脸书发表长文，以“花园的地上有一颗鸡蛋”为题，将香港年轻人形容为鸡蛋，当局则是“铜墙铁壁”，呼吁要“轻轻拾起，捧在手心”。《人民日报》随即发表题为“为何只见鸡蛋不见燃烧弹？”的评论，称年轻人是“铁锤”，指龙应台将矛头指向内地，“挑唆香港和内地的关系”。



目前，**京东搜索“龙应台”及相关作品已显示无结果**。当当网则是屏蔽了“龙应台”字眼的搜索结果，但搜索作品名称或“龙应台 xxx”时仍可搜到相关商品。



## 吹哨人



一份《国务院关于加强和规范事中事后监管的指导意见》，把“吹哨人”这个名字带到了我们面前。文件写明，**“建立‘吹哨人’、内部举报人等制度，对举报严重违法违规行为和重大风险隐患的有功人员予以重奖和严格保护”**。

《南方日报》其后发布评论《“吹哨人”有了名分，还缺什么？》称，“吹哨人”应当放下“告密”的心理包袱，并坦然接受奖励。这引发了大家对于“告密”的担心，在现实的情况下，“吹哨人”制度是会成为推动行业发展的有力武器，还是会让人人自危？我们不得而知。



![](https://assets.matters.news/embed/bc7b68ba-d901-4805-b8b5-d79e1587aad9.webp)

《国务院关于加强和规范事中事后监管的指导意见》截图

但无论如何，“发动群众的力量”似乎效果真的不错。根据澎湃新闻的消息，**上海网信办本月招收了首批网络举报志愿者，共349名。**网络志愿者招募是“争做中国好网民上海网民在行动”有关“网络乱象我举报”活动的组成部分，网信办还组织了网络举报志愿者培训班。

澎湃新闻形容，这支队伍“覆盖各行业、各领域、以及各年龄段”。上海网信办透露，截至9月26日，上海市互联网违法和不良信息举报中心已接到网络举报志愿者举报线索1351条。



除了发动群众的力量，政府也会“亲自”出手。杭州市的“新制造业计划”选取了100名机关干部，作为“**政府事务代表**”，派驻阿里、娃哈哈等100家重点企业，“服务重点企业，为企业协调解决各类政府事务、开展信息沟通交流、政策解答和项目落地推进提供等全方位的保障”。该措施引致网友质疑“政府手伸太长”，杭州市政府则回应称，“政府事务代表”是在充分尊重企业意愿的基础上派驻，不参与企业日常经营管理。



## 全球观察



除大陆与香港外，本月全球亦发生了不少值得我们关注的事件。



**台湾：**南太平洋上的两个岛国——所罗门群岛和基里巴斯在16日和20日先后宣布与台湾断交，蔡英文随后则表示，面对外交打压，台湾绝不妥协；香港导演杜琪峰辞去了原定担任的第56届金马奖评审团主席一职，理由是**“电影投资方的合约限制”**。



**伊朗：**据伊朗本地媒体，29岁的女孩莎哈3月的时候，因试图假扮男性进入阿扎迪体育场看足球，被道德警察逮捕。本月，怀疑因得知自己可能获刑6个月，**她在法院外自焚，最终不幸离世。**她被人们称为“蓝色女孩”。国际足联在记者会上表示，在经过与伊朗当局的讨论后，**从下个月10日的一场比赛开始，伊朗女性将被允许进入球场观赛。**

![](https://assets.matters.news/embed/431f954f-015a-4b50-881e-af5d87957a12.webp)

莎哈支持球队的球迷在观看比赛，图自卫报

**美国：**众议院对总统特朗普开展了弹劾调查，原因是**特朗普被匿名举报在与乌克兰总统的通话中向其施压，要求对方调查下届总统竞选对手拜登及其儿子。**此举被认为滥用权力谋取私利。

**斯诺登的新书《永久记录》(Permanent Record)9月17日在美国出版。**但很快，美国司法部便对其发起民事诉讼，指该书的出版违法了斯诺登与中情局及国家安全局的保密协议，要求收回斯诺登从该书中获取的利润，并禁止出版公司向斯诺登转送资金。斯诺登对此则表示，司法部的诉讼证明了该书内容的真实性。



**俄罗斯：**俄罗斯联邦电信、信息技术与大众传媒监督局8日发声明称，**谷歌的搜索引擎及旗下视频网站Youtube、脸书在选举日投放政治广告，干涉了俄罗斯的地方议会选举。**由于此前莫斯科多名反对派候选人被取消选举资格，俄罗斯已持续超过一个月出现抗议活动。



据RBC通讯社报道，**俄罗斯计划于九月底到十月测试“俄罗斯互联网”**，相关设备已在主要电信运营商的网络上安装完成。根据5月通过的一条法案，俄罗斯需安装一个新的互联网基础设施，以确保俄罗斯互联网可以在不依赖外国服务器的情况下运作。

**校对：阿九**