---
templateKey: article
title: 圣经是女性主义的敌人？《使女的故事》这锅，宗教不背！
slug: sheng-jing-shi-nu-xing-zhu-yi-de-di-ren-shi-nu-de-gu-shi-zhe-guo-zong-jiao-bu-bei
date: 2020-03-30T09:02:17.221Z
featuredpost: true
trending: 7
isbrief: true
columnist: 作家009
tags:
  - 好的
---
作者：Jane

最近，美剧《使女的故事》火热，这部片包含了极权、女性压迫、宗教元素，剧评指片中的世界里，女人成了带腿的子宫。

而这种压迫则依靠"宗教教义"得以正当化，不过，本文作者却提出"这锅宗教不背"的"异议"，来听听文化与宗教的研究者是如何分析《使女的故事》：

![](http://pic.caixin.com/blog/Mon_1705/m_1494930708_IYfdbP.jpg)

电视剧《使女的故事》改编自玛格丽特·阿特伍德的同名小说。故事发生在 "基列共和国"，一个政教合一并以父权主宰一切的社会：女性被严苛控制，不能工作和拥有财产，依照剩余价值被分配不同任务。

其中一些有生育能力的女性被迫成为"使女"，使女被分配到统治社会的大主教的家中，代替不孕的夫人，与大主教交合，为他们繁衍后代。故事的女主角正是使女之一，更透过她的经历反映使女们被剥夺自由、身份和情欲的悲惨遭遇。

《使女的故事》虽说是虚构小说，描写的反乌托邦国度也异常夸张，似乎只有在极其极端的自然地理和社会条件下才能产生，却能让观者感到"真实的毛骨悚然"，我想主要原因有二。

一、这个故事结合了现代社会两大毒瘤：集权主义与宗教极端主义；二、作者自述，故事中的片段情节都在人类历史上有迹可循。

而八十年代面临的种种社会风潮，无论是生态危机、环境污染、经济危机以及多元价值的萌发，如今依旧发酵，出于对恐怖主义的恐惧，部分地区的人民逐渐愿意把自己的权利交还政府，而禁锢女性的原教旨主义思想抬头。这些都已经不是预言，而是当下。

关于集权主义的批判解读已有许多专著文学作品，本文不再赘述，想要谈谈的是原教旨主义的问题。

**原教旨主义是什么？**

宗教原教旨主义说起来离我们不远，无论是基督教中的基要派，美国的宗教右派（即作者假想中的基列国的前身），还是伊斯兰国，其实都是一种原教旨主义。

根据《牛津简明世界宗教词典》的定义，原教旨主义指的是那些认为信仰应回归最基本真理和信仰实践的人，在基督教、伊斯兰教、佛教等诸多宗教都有此派别。而就基督教来说，原教旨主义者特别捍卫对圣经的字面理解，反对任何从历史或神学角度出发的圣经批判。

这样看来，原教旨主义者似乎正如他们所说，最忠于宗教信仰，甚至到了追根溯源的地步，也最尊重信仰，遵从经典的字字句句而拒绝任何诠释（即妥协）空间，也只有他们掌握了上帝的真理。

然而，任何一个宗教，只要是在前现代社会时期成型的宗教，翻阅其经典或创教者的言行，必然有着大量在如今看来是政治不正确，甚至反社会，反人类，反文明的言论。

这是否代表任何宗教，其实都是禁锢现代人，特别是在剧集的境况中，压迫女性的呢？

我只能说，这个锅，任何宗教都不背。

**大主教们的生育政策，有圣经根据吗？**

回到《使女的故事》，基列国的生育政策，将有生育能力的女性分配给大主教作为生育工具，为他们不孕的妻子生下后代，其理据正是来自于圣经旧约。

原教旨主义者既要解决生育率底下的问题，又必须捍卫一夫一妻的神圣家庭价值，因此使女的地位既不能是小妾，也不是二房，只是一个生育工具，一个行走子宫。

大主教和使女之间的性行为不仅有妻子共同参与，还要举行宗教仪式一般的受精仪式，所有家庭成员，包括马大、司机们都必须参加，如同礼拜一样，各人依其身份而站立在固定的位置上，大主教从上锁的盒子中取出圣经，像牧师一样诵读神圣的经文。

这种受精仪式，正是透过其仪式感和神圣感，合理化对使女的性剥削，更进一步将使女去人化--做爱、交欢等等描写性行为的词语在此处都不适用，因为这种关系中女方毫无能动性，双方除了性接触没有任何交流。使女只是一个容器，大主教只是在进行抽插动作，然后将精子放置到这个容器中，使之培育出自己的后代。虽然一系列的生理动作若是分隔成不同的环节，和正常的男女性交没有任何分别，但正是因为交流和联系的刻意缺失阻隔，才将使女物化。

拉结见自己不给雅各生子，就嫉妒她姐姐，对雅各说，你给我孩子，不然我就死了。雅各向拉结生气，说，叫你不生育的是神，我岂能代替他作主呢？拉结说，有我的使女辟拉在这里，你可以与她同房，使她生子在我膝下，我便因她也得孩子（得孩子原文作被建立）。拉结就把她的使女辟拉给丈夫为妾。雅各便与她同房，辟拉就怀孕，给雅各生了一个儿子。（创世纪30章1-5节）

注：雅各与拉结为圣经创世纪中的人物。雅各被认为是以色列人的祖先，他先后娶拉结的姐姐利亚，以及拉结为妻子。

从原教旨主义者的角度来说，这段经文正是佐证，说明授精仪式的宗教合法性。他们相信，一个教徒的生活方方面面都必须符合神的旨意，婚姻、家庭和生育后代是神的祝福和命令，而当现实出现缺陷的时候，神也自然启示了变通之道。

既然原教旨主义者认为不需要考虑作者处境和读者处境，就必须将经典全盘接受，那我也不在此赘述以使女代替正房，其实是近东游牧民族出于繁衍后代的生存需求，才出现的特殊家庭现象。

不如我回到圣经中，来看看雅各的祖父，亚伯拉罕，他的妻子撒拉也曾经不孕，甚至过了生育的年龄还是没有子嗣，而更糟糕的是，与雅各的情况不同，亚伯拉罕可是得到了上帝的一再启示，甚至是保证，他的后代会如海边的沙天上的星那么多。那么既然撒拉不孕，是不是上帝的意思是，撒拉可以从她的使女那里也得到一个上帝应许的孩子呢？

注：亚伯拉罕和撒拉为圣经创世纪中的人物。亚伯拉罕是雅各的爷爷。他和他的妻子的原名为亚伯兰和撒莱，后上帝耶和华为他们重新起名为亚伯拉罕和撒拉。

（耶和华）于是领他（亚伯拉罕）走到外边，说，你向天观看，数算众星，能数得过来吗？又对他说，你的后裔将要如此。（创世纪15章5节）

亚伯兰的妻子撒莱不给他生儿女。撒莱有一个使女，名叫夏甲，是埃及人。撒莱对亚伯兰说，耶和华使我不能生育。求你和我的使女同房，或者我可以因她得孩子（得孩子原文作被建立）。亚伯兰听从了撒莱的话。于是亚伯兰的妻子撒莱将使女埃及人夏甲给了丈夫为妾。那时亚伯兰在迦南已经住了十年。亚伯兰与夏甲同房，夏甲就怀了孕。（创世纪16章1-4节）

亚伯拉罕就俯伏在地喜笑，心里说，一百岁的人还能得孩子吗？撒拉已经九十岁了，还能生养吗？亚伯拉罕对神说，但愿以实玛利活在你面前。神说，不然，你妻子撒拉要给你生一个儿子，你要给他起名叫以撒。我要与他坚定所立的约，作他后裔永远的约。（创世纪17章17-19节）

注：以实玛利为亚伯拉罕与夏甲所生的儿子

亚伯拉罕和雅各一样，他们的妻子都不孕，而撒拉的年龄甚至更大，已经断了经期。然而，当亚伯拉罕以民俗娶妻子的使女为妾生子之后，上帝却说，他所应许的儿子不是从妾所得，而必须是从撒拉所得。

当基列国的统治者以雅各和拉结的故事来粉饰自己对女性剥削的时候，同一本圣经，就在前几章的故事里，却传达出完全不同的意思。如果按照亚伯拉罕的版本，使女们生出的孩子根本不能算是主教们的后代，也更不是他们口口声声所说的上帝的祝福，蒙神喜悦的方法，应该是继续祈祷等待，一直等到天荒地老，等待奇迹降临，既然上帝能让九十九岁的不孕女性怀孕，对那些太太们，不就更没难度了吗？

横向来说，原教旨主义者就是在打自己的脸，那么纵向来看，其实原教旨主义者的道德标准，不仅仅是双标，而是一直都在变。

比如说，今日的原教旨主义者，也就是基列国的前辈们，在干什么呢？他们像《使女的故事》里一样，反对堕胎、反对同性恋、反对婚前性行为等等，但诸如基列国发生的事情要是让他们知道了，肯定立马高举一男一女一夫一妻一生一世的大旗，要向基列国发起宗教战争了。

在基列国，这是上帝的祝福，是神圣的使命；而在当下，这叫做淫乱。

当原教旨主义者们指着圣经说，圣经里面明明白白写着上帝憎恨同性恋，你也翻出圣经指着创世纪说，那么不孕不育的夫妻是不是可以找个小三来代孕，他们必定立马说，这些经文是有历史背景的，不能从字面上来理解，按照圣经中的教导，家庭必须只有一男一女blahblahblah。

所以你看，原教旨主义者们并非不相信历史背景，原教旨主义者们也不是真正相信经典的一切都可按字面奉行。

经文是神圣的，不过某些经文，比另一些经文更神圣；经文都是上帝启示的，不过某些经文，比另一些经文是更重要的启示。而究竟哪些更神圣，哪些更重要，哪些是更需要暴力贯彻的启示，重点还是看话语权在谁的手中。

因此，在基列国的权力结构中，女性被剥夺阅读和识字的权力，以防她们能直接接触到真理而挑战权威解释；女性必须沉静恭顺，异见者除了手脚被拘束，口也被封住，生怕她们发出另类的声音。

所以说，所谓原教旨主义根本就是个伪命题，只是一个冠冕堂皇的掩饰，真正说话的，是躲在背后的权力结构。

**宗教并非女性的枷锁，父权主义才是**

于前现代的，父权的社会背景下写就的圣经，以及其他宗教经典，必然带有浓重的父权思想的沉渣，但即便高举圣经字句的原教旨主义者们，也不得不承认，随着社会思潮的发展和进步，随着对人权、平等、自由等现代思想的启蒙，没有任何宗教信仰可以完全地照搬经文来实践。

原教旨主义者或许会抓住圣经字句，反对同性恋、要求妻子顺服丈夫，但圣经中同样记载的，女人要蒙头，女人必须留长发，这些因为太落后于时代而连他们都得扬弃了。而至于女性是否可以讲道，担任圣职这样争议，虽然不同教派之间仍有争论，但比起写作年代的完全禁止，已经是进步了许多。

《使女的故事》写出的是一个以原教旨主义控制社会的集权政治模式，似乎让人觉得，就算宗教本身不是恶意的，但如果能被用来当成杀人工具，不如索性不要罢了。

但事实上，宗教并非没有主动性，只能容人随意装扮，宗教同样能够赋权于人，催人进步和解放。

正如十九世纪的基督新教，当它传入男权主义家长制的中国社会时，不仅没有被中国文化所同化，反而推动了中国女性地位提升和觉醒的第一步。基督教创办的育婴堂专收留被父母遗弃的女婴，将之养育长大并提供良好教育；不少教会学校是最早向男女开放的现代学校，更创办大量女子学校，补足私塾教育重男轻女的不足。

在女子学校中，学生们接受的除了宗教知识，更有现代科学知识和外语能力，使之谋得可以经济独立的能力，另一方面，西式学校注重体育、慈善等活动，促使女性更主动地参与社会与政治，成为变革中的中国中一股新的势力和声音。不少教会学校毕业的女性都表示，正是看到学校中来华传教或教书的独身女性，让她们渴望命运自主，并看到了相夫教子之外的其他想象，得以不被传统的家庭观念所禁锢，勇敢追寻自己的未来。

而尽管基督教会传统上一直扮演着反对同性恋的角色，但随着圣经诠释学和神学的发展，不少神学家和信徒开始反思，圣经中关于反同性恋的经文背后的真正涵义。

从历史处境和原文的角度重新解释，不仅为同性恋正名，更发展成了当代神学的显学：同志神学和酷儿神学，从性别认同被边缘化的群体的角度理解信仰。在同志平权运动之中，也有基督教会的身影，2013年香港同志友善教会就发起"彩虹之约"联署，呼吁教会接纳同志并支持争取平权运动。

在美国，不少教会不仅对同志的态度越来越正面和开放，甚至已经开始允许同志担任牧師，连传统大公教会圣公会在2003年都已经选出了第一位同志主教。这些例子正体现宗教并非腐朽不变，它同样可以与时俱进，甚至走在时代前列，实践对全人类和整个社会的博爱关怀。

无论是《使女的故事》中的基列国，还是当下宗教极端主义的阴云，都令人恐惧警惕。

但是，我们所要对抗的不是宗教本身，也不是某种宗教主义，而是躲在这些背后真正的敌人--根深蒂固的父权主义。无论是在宗教内，还是宗教外，发出女性自己的声音，发出挑战父权权力结构的声音，打破它的幽灵，而非皮相，才能自救。

本文作者系香港中文大学文化与宗教研究系博士候选人，生于上海，现居香港。研究领域：基督教历史、区域历史、民族认同与历史记忆

作者：Jane

最近，美剧《使女的故事》火热，这部片包含了极权、女性压迫、宗教元素，剧评指片中的世界里，女人成了带腿的子宫。

而这种压迫则依靠"宗教教义"得以正当化，不过，本文作者却提出"这锅宗教不背"的"异议"，来听听文化与宗教的研究者是如何分析《使女的故事》：

![](http://pic.caixin.com/blog/Mon_1705/m_1494930708_IYfdbP.jpg)

电视剧《使女的故事》改编自玛格丽特·阿特伍德的同名小说。故事发生在 "基列共和国"，一个政教合一并以父权主宰一切的社会：女性被严苛控制，不能工作和拥有财产，依照剩余价值被分配不同任务。

其中一些有生育能力的女性被迫成为"使女"，使女被分配到统治社会的大主教的家中，代替不孕的夫人，与大主教交合，为他们繁衍后代。故事的女主角正是使女之一，更透过她的经历反映使女们被剥夺自由、身份和情欲的悲惨遭遇。

《使女的故事》虽说是虚构小说，描写的反乌托邦国度也异常夸张，似乎只有在极其极端的自然地理和社会条件下才能产生，却能让观者感到"真实的毛骨悚然"，我想主要原因有二。

一、这个故事结合了现代社会两大毒瘤：集权主义与宗教极端主义；二、作者自述，故事中的片段情节都在人类历史上有迹可循。

而八十年代面临的种种社会风潮，无论是生态危机、环境污染、经济危机以及多元价值的萌发，如今依旧发酵，出于对恐怖主义的恐惧，部分地区的人民逐渐愿意把自己的权利交还政府，而禁锢女性的原教旨主义思想抬头。这些都已经不是预言，而是当下。

关于集权主义的批判解读已有许多专著文学作品，本文不再赘述，想要谈谈的是原教旨主义的问题。

**原教旨主义是什么？**

宗教原教旨主义说起来离我们不远，无论是基督教中的基要派，美国的宗教右派（即作者假想中的基列国的前身），还是伊斯兰国，其实都是一种原教旨主义。

根据《牛津简明世界宗教词典》的定义，原教旨主义指的是那些认为信仰应回归最基本真理和信仰实践的人，在基督教、伊斯兰教、佛教等诸多宗教都有此派别。而就基督教来说，原教旨主义者特别捍卫对圣经的字面理解，反对任何从历史或神学角度出发的圣经批判。

这样看来，原教旨主义者似乎正如他们所说，最忠于宗教信仰，甚至到了追根溯源的地步，也最尊重信仰，遵从经典的字字句句而拒绝任何诠释（即妥协）空间，也只有他们掌握了上帝的真理。

然而，任何一个宗教，只要是在前现代社会时期成型的宗教，翻阅其经典或创教者的言行，必然有着大量在如今看来是政治不正确，甚至反社会，反人类，反文明的言论。

这是否代表任何宗教，其实都是禁锢现代人，特别是在剧集的境况中，压迫女性的呢？

我只能说，这个锅，任何宗教都不背。

**大主教们的生育政策，有圣经根据吗？**

回到《使女的故事》，基列国的生育政策，将有生育能力的女性分配给大主教作为生育工具，为他们不孕的妻子生下后代，其理据正是来自于圣经旧约。

原教旨主义者既要解决生育率底下的问题，又必须捍卫一夫一妻的神圣家庭价值，因此使女的地位既不能是小妾，也不是二房，只是一个生育工具，一个行走子宫。

大主教和使女之间的性行为不仅有妻子共同参与，还要举行宗教仪式一般的受精仪式，所有家庭成员，包括马大、司机们都必须参加，如同礼拜一样，各人依其身份而站立在固定的位置上，大主教从上锁的盒子中取出圣经，像牧师一样诵读神圣的经文。

这种受精仪式，正是透过其仪式感和神圣感，合理化对使女的性剥削，更进一步将使女去人化--做爱、交欢等等描写性行为的词语在此处都不适用，因为这种关系中女方毫无能动性，双方除了性接触没有任何交流。使女只是一个容器，大主教只是在进行抽插动作，然后将精子放置到这个容器中，使之培育出自己的后代。虽然一系列的生理动作若是分隔成不同的环节，和正常的男女性交没有任何分别，但正是因为交流和联系的刻意缺失阻隔，才将使女物化。

拉结见自己不给雅各生子，就嫉妒她姐姐，对雅各说，你给我孩子，不然我就死了。雅各向拉结生气，说，叫你不生育的是神，我岂能代替他作主呢？拉结说，有我的使女辟拉在这里，你可以与她同房，使她生子在我膝下，我便因她也得孩子（得孩子原文作被建立）。拉结就把她的使女辟拉给丈夫为妾。雅各便与她同房，辟拉就怀孕，给雅各生了一个儿子。（创世纪30章1-5节）

注：雅各与拉结为圣经创世纪中的人物。雅各被认为是以色列人的祖先，他先后娶拉结的姐姐利亚，以及拉结为妻子。

从原教旨主义者的角度来说，这段经文正是佐证，说明授精仪式的宗教合法性。他们相信，一个教徒的生活方方面面都必须符合神的旨意，婚姻、家庭和生育后代是神的祝福和命令，而当现实出现缺陷的时候，神也自然启示了变通之道。

既然原教旨主义者认为不需要考虑作者处境和读者处境，就必须将经典全盘接受，那我也不在此赘述以使女代替正房，其实是近东游牧民族出于繁衍后代的生存需求，才出现的特殊家庭现象。

不如我回到圣经中，来看看雅各的祖父，亚伯拉罕，他的妻子撒拉也曾经不孕，甚至过了生育的年龄还是没有子嗣，而更糟糕的是，与雅各的情况不同，亚伯拉罕可是得到了上帝的一再启示，甚至是保证，他的后代会如海边的沙天上的星那么多。那么既然撒拉不孕，是不是上帝的意思是，撒拉可以从她的使女那里也得到一个上帝应许的孩子呢？

注：亚伯拉罕和撒拉为圣经创世纪中的人物。亚伯拉罕是雅各的爷爷。他和他的妻子的原名为亚伯兰和撒莱，后上帝耶和华为他们重新起名为亚伯拉罕和撒拉。

（耶和华）于是领他（亚伯拉罕）走到外边，说，你向天观看，数算众星，能数得过来吗？又对他说，你的后裔将要如此。（创世纪15章5节）

亚伯兰的妻子撒莱不给他生儿女。撒莱有一个使女，名叫夏甲，是埃及人。撒莱对亚伯兰说，耶和华使我不能生育。求你和我的使女同房，或者我可以因她得孩子（得孩子原文作被建立）。亚伯兰听从了撒莱的话。于是亚伯兰的妻子撒莱将使女埃及人夏甲给了丈夫为妾。那时亚伯兰在迦南已经住了十年。亚伯兰与夏甲同房，夏甲就怀了孕。（创世纪16章1-4节）

亚伯拉罕就俯伏在地喜笑，心里说，一百岁的人还能得孩子吗？撒拉已经九十岁了，还能生养吗？亚伯拉罕对神说，但愿以实玛利活在你面前。神说，不然，你妻子撒拉要给你生一个儿子，你要给他起名叫以撒。我要与他坚定所立的约，作他后裔永远的约。（创世纪17章17-19节）

注：以实玛利为亚伯拉罕与夏甲所生的儿子

亚伯拉罕和雅各一样，他们的妻子都不孕，而撒拉的年龄甚至更大，已经断了经期。然而，当亚伯拉罕以民俗娶妻子的使女为妾生子之后，上帝却说，他所应许的儿子不是从妾所得，而必须是从撒拉所得。

当基列国的统治者以雅各和拉结的故事来粉饰自己对女性剥削的时候，同一本圣经，就在前几章的故事里，却传达出完全不同的意思。如果按照亚伯拉罕的版本，使女们生出的孩子根本不能算是主教们的后代，也更不是他们口口声声所说的上帝的祝福，蒙神喜悦的方法，应该是继续祈祷等待，一直等到天荒地老，等待奇迹降临，既然上帝能让九十九岁的不孕女性怀孕，对那些太太们，不就更没难度了吗？

横向来说，原教旨主义者就是在打自己的脸，那么纵向来看，其实原教旨主义者的道德标准，不仅仅是双标，而是一直都在变。

比如说，今日的原教旨主义者，也就是基列国的前辈们，在干什么呢？他们像《使女的故事》里一样，反对堕胎、反对同性恋、反对婚前性行为等等，但诸如基列国发生的事情要是让他们知道了，肯定立马高举一男一女一夫一妻一生一世的大旗，要向基列国发起宗教战争了。

在基列国，这是上帝的祝福，是神圣的使命；而在当下，这叫做淫乱。

当原教旨主义者们指着圣经说，圣经里面明明白白写着上帝憎恨同性恋，你也翻出圣经指着创世纪说，那么不孕不育的夫妻是不是可以找个小三来代孕，他们必定立马说，这些经文是有历史背景的，不能从字面上来理解，按照圣经中的教导，家庭必须只有一男一女blahblahblah。

所以你看，原教旨主义者们并非不相信历史背景，原教旨主义者们也不是真正相信经典的一切都可按字面奉行。

经文是神圣的，不过某些经文，比另一些经文更神圣；经文都是上帝启示的，不过某些经文，比另一些经文是更重要的启示。而究竟哪些更神圣，哪些更重要，哪些是更需要暴力贯彻的启示，重点还是看话语权在谁的手中。

因此，在基列国的权力结构中，女性被剥夺阅读和识字的权力，以防她们能直接接触到真理而挑战权威解释；女性必须沉静恭顺，异见者除了手脚被拘束，口也被封住，生怕她们发出另类的声音。

所以说，所谓原教旨主义根本就是个伪命题，只是一个冠冕堂皇的掩饰，真正说话的，是躲在背后的权力结构。

**宗教并非女性的枷锁，父权主义才是**

于前现代的，父权的社会背景下写就的圣经，以及其他宗教经典，必然带有浓重的父权思想的沉渣，但即便高举圣经字句的原教旨主义者们，也不得不承认，随着社会思潮的发展和进步，随着对人权、平等、自由等现代思想的启蒙，没有任何宗教信仰可以完全地照搬经文来实践。

原教旨主义者或许会抓住圣经字句，反对同性恋、要求妻子顺服丈夫，但圣经中同样记载的，女人要蒙头，女人必须留长发，这些因为太落后于时代而连他们都得扬弃了。而至于女性是否可以讲道，担任圣职这样争议，虽然不同教派之间仍有争论，但比起写作年代的完全禁止，已经是进步了许多。

《使女的故事》写出的是一个以原教旨主义控制社会的集权政治模式，似乎让人觉得，就算宗教本身不是恶意的，但如果能被用来当成杀人工具，不如索性不要罢了。

但事实上，宗教并非没有主动性，只能容人随意装扮，宗教同样能够赋权于人，催人进步和解放。

正如十九世纪的基督新教，当它传入男权主义家长制的中国社会时，不仅没有被中国文化所同化，反而推动了中国女性地位提升和觉醒的第一步。基督教创办的育婴堂专收留被父母遗弃的女婴，将之养育长大并提供良好教育；不少教会学校是最早向男女开放的现代学校，更创办大量女子学校，补足私塾教育重男轻女的不足。

在女子学校中，学生们接受的除了宗教知识，更有现代科学知识和外语能力，使之谋得可以经济独立的能力，另一方面，西式学校注重体育、慈善等活动，促使女性更主动地参与社会与政治，成为变革中的中国中一股新的势力和声音。不少教会学校毕业的女性都表示，正是看到学校中来华传教或教书的独身女性，让她们渴望命运自主，并看到了相夫教子之外的其他想象，得以不被传统的家庭观念所禁锢，勇敢追寻自己的未来。

而尽管基督教会传统上一直扮演着反对同性恋的角色，但随着圣经诠释学和神学的发展，不少神学家和信徒开始反思，圣经中关于反同性恋的经文背后的真正涵义。

从历史处境和原文的角度重新解释，不仅为同性恋正名，更发展成了当代神学的显学：同志神学和酷儿神学，从性别认同被边缘化的群体的角度理解信仰。在同志平权运动之中，也有基督教会的身影，2013年香港同志友善教会就发起"彩虹之约"联署，呼吁教会接纳同志并支持争取平权运动。

在美国，不少教会不仅对同志的态度越来越正面和开放，甚至已经开始允许同志担任牧師，连传统大公教会圣公会在2003年都已经选出了第一位同志主教。这些例子正体现宗教并非腐朽不变，它同样可以与时俱进，甚至走在时代前列，实践对全人类和整个社会的博爱关怀。

无论是《使女的故事》中的基列国，还是当下宗教极端主义的阴云，都令人恐惧警惕。

但是，我们所要对抗的不是宗教本身，也不是某种宗教主义，而是躲在这些背后真正的敌人--根深蒂固的父权主义。无论是在宗教内，还是宗教外，发出女性自己的声音，发出挑战父权权力结构的声音，打破它的幽灵，而非皮相，才能自救。

本文作者系香港中文大学文化与宗教研究系博士候选人，生于上海，现居香港。研究领域：基督教历史、区域历史、民族认同与历史记忆

作者：Jane

最近，美剧《使女的故事》火热，这部片包含了极权、女性压迫、宗教元素，剧评指片中的世界里，女人成了带腿的子宫。

而这种压迫则依靠"宗教教义"得以正当化，不过，本文作者却提出"这锅宗教不背"的"异议"，来听听文化与宗教的研究者是如何分析《使女的故事》：

![](http://pic.caixin.com/blog/Mon_1705/m_1494930708_IYfdbP.jpg)

电视剧《使女的故事》改编自玛格丽特·阿特伍德的同名小说。故事发生在 "基列共和国"，一个政教合一并以父权主宰一切的社会：女性被严苛控制，不能工作和拥有财产，依照剩余价值被分配不同任务。

其中一些有生育能力的女性被迫成为"使女"，使女被分配到统治社会的大主教的家中，代替不孕的夫人，与大主教交合，为他们繁衍后代。故事的女主角正是使女之一，更透过她的经历反映使女们被剥夺自由、身份和情欲的悲惨遭遇。

《使女的故事》虽说是虚构小说，描写的反乌托邦国度也异常夸张，似乎只有在极其极端的自然地理和社会条件下才能产生，却能让观者感到"真实的毛骨悚然"，我想主要原因有二。

一、这个故事结合了现代社会两大毒瘤：集权主义与宗教极端主义；二、作者自述，故事中的片段情节都在人类历史上有迹可循。

而八十年代面临的种种社会风潮，无论是生态危机、环境污染、经济危机以及多元价值的萌发，如今依旧发酵，出于对恐怖主义的恐惧，部分地区的人民逐渐愿意把自己的权利交还政府，而禁锢女性的原教旨主义思想抬头。这些都已经不是预言，而是当下。

关于集权主义的批判解读已有许多专著文学作品，本文不再赘述，想要谈谈的是原教旨主义的问题。

**原教旨主义是什么？**

宗教原教旨主义说起来离我们不远，无论是基督教中的基要派，美国的宗教右派（即作者假想中的基列国的前身），还是伊斯兰国，其实都是一种原教旨主义。

根据《牛津简明世界宗教词典》的定义，原教旨主义指的是那些认为信仰应回归最基本真理和信仰实践的人，在基督教、伊斯兰教、佛教等诸多宗教都有此派别。而就基督教来说，原教旨主义者特别捍卫对圣经的字面理解，反对任何从历史或神学角度出发的圣经批判。

这样看来，原教旨主义者似乎正如他们所说，最忠于宗教信仰，甚至到了追根溯源的地步，也最尊重信仰，遵从经典的字字句句而拒绝任何诠释（即妥协）空间，也只有他们掌握了上帝的真理。

然而，任何一个宗教，只要是在前现代社会时期成型的宗教，翻阅其经典或创教者的言行，必然有着大量在如今看来是政治不正确，甚至反社会，反人类，反文明的言论。

这是否代表任何宗教，其实都是禁锢现代人，特别是在剧集的境况中，压迫女性的呢？

我只能说，这个锅，任何宗教都不背。

**大主教们的生育政策，有圣经根据吗？**

回到《使女的故事》，基列国的生育政策，将有生育能力的女性分配给大主教作为生育工具，为他们不孕的妻子生下后代，其理据正是来自于圣经旧约。

原教旨主义者既要解决生育率底下的问题，又必须捍卫一夫一妻的神圣家庭价值，因此使女的地位既不能是小妾，也不是二房，只是一个生育工具，一个行走子宫。

大主教和使女之间的性行为不仅有妻子共同参与，还要举行宗教仪式一般的受精仪式，所有家庭成员，包括马大、司机们都必须参加，如同礼拜一样，各人依其身份而站立在固定的位置上，大主教从上锁的盒子中取出圣经，像牧师一样诵读神圣的经文。

这种受精仪式，正是透过其仪式感和神圣感，合理化对使女的性剥削，更进一步将使女去人化--做爱、交欢等等描写性行为的词语在此处都不适用，因为这种关系中女方毫无能动性，双方除了性接触没有任何交流。使女只是一个容器，大主教只是在进行抽插动作，然后将精子放置到这个容器中，使之培育出自己的后代。虽然一系列的生理动作若是分隔成不同的环节，和正常的男女性交没有任何分别，但正是因为交流和联系的刻意缺失阻隔，才将使女物化。

拉结见自己不给雅各生子，就嫉妒她姐姐，对雅各说，你给我孩子，不然我就死了。雅各向拉结生气，说，叫你不生育的是神，我岂能代替他作主呢？拉结说，有我的使女辟拉在这里，你可以与她同房，使她生子在我膝下，我便因她也得孩子（得孩子原文作被建立）。拉结就把她的使女辟拉给丈夫为妾。雅各便与她同房，辟拉就怀孕，给雅各生了一个儿子。（创世纪30章1-5节）

注：雅各与拉结为圣经创世纪中的人物。雅各被认为是以色列人的祖先，他先后娶拉结的姐姐利亚，以及拉结为妻子。

从原教旨主义者的角度来说，这段经文正是佐证，说明授精仪式的宗教合法性。他们相信，一个教徒的生活方方面面都必须符合神的旨意，婚姻、家庭和生育后代是神的祝福和命令，而当现实出现缺陷的时候，神也自然启示了变通之道。

既然原教旨主义者认为不需要考虑作者处境和读者处境，就必须将经典全盘接受，那我也不在此赘述以使女代替正房，其实是近东游牧民族出于繁衍后代的生存需求，才出现的特殊家庭现象。

不如我回到圣经中，来看看雅各的祖父，亚伯拉罕，他的妻子撒拉也曾经不孕，甚至过了生育的年龄还是没有子嗣，而更糟糕的是，与雅各的情况不同，亚伯拉罕可是得到了上帝的一再启示，甚至是保证，他的后代会如海边的沙天上的星那么多。那么既然撒拉不孕，是不是上帝的意思是，撒拉可以从她的使女那里也得到一个上帝应许的孩子呢？

注：亚伯拉罕和撒拉为圣经创世纪中的人物。亚伯拉罕是雅各的爷爷。他和他的妻子的原名为亚伯兰和撒莱，后上帝耶和华为他们重新起名为亚伯拉罕和撒拉。

（耶和华）于是领他（亚伯拉罕）走到外边，说，你向天观看，数算众星，能数得过来吗？又对他说，你的后裔将要如此。（创世纪15章5节）

亚伯兰的妻子撒莱不给他生儿女。撒莱有一个使女，名叫夏甲，是埃及人。撒莱对亚伯兰说，耶和华使我不能生育。求你和我的使女同房，或者我可以因她得孩子（得孩子原文作被建立）。亚伯兰听从了撒莱的话。于是亚伯兰的妻子撒莱将使女埃及人夏甲给了丈夫为妾。那时亚伯兰在迦南已经住了十年。亚伯兰与夏甲同房，夏甲就怀了孕。（创世纪16章1-4节）

亚伯拉罕就俯伏在地喜笑，心里说，一百岁的人还能得孩子吗？撒拉已经九十岁了，还能生养吗？亚伯拉罕对神说，但愿以实玛利活在你面前。神说，不然，你妻子撒拉要给你生一个儿子，你要给他起名叫以撒。我要与他坚定所立的约，作他后裔永远的约。（创世纪17章17-19节）

注：以实玛利为亚伯拉罕与夏甲所生的儿子

亚伯拉罕和雅各一样，他们的妻子都不孕，而撒拉的年龄甚至更大，已经断了经期。然而，当亚伯拉罕以民俗娶妻子的使女为妾生子之后，上帝却说，他所应许的儿子不是从妾所得，而必须是从撒拉所得。

当基列国的统治者以雅各和拉结的故事来粉饰自己对女性剥削的时候，同一本圣经，就在前几章的故事里，却传达出完全不同的意思。如果按照亚伯拉罕的版本，使女们生出的孩子根本不能算是主教们的后代，也更不是他们口口声声所说的上帝的祝福，蒙神喜悦的方法，应该是继续祈祷等待，一直等到天荒地老，等待奇迹降临，既然上帝能让九十九岁的不孕女性怀孕，对那些太太们，不就更没难度了吗？

横向来说，原教旨主义者就是在打自己的脸，那么纵向来看，其实原教旨主义者的道德标准，不仅仅是双标，而是一直都在变。

比如说，今日的原教旨主义者，也就是基列国的前辈们，在干什么呢？他们像《使女的故事》里一样，反对堕胎、反对同性恋、反对婚前性行为等等，但诸如基列国发生的事情要是让他们知道了，肯定立马高举一男一女一夫一妻一生一世的大旗，要向基列国发起宗教战争了。

在基列国，这是上帝的祝福，是神圣的使命；而在当下，这叫做淫乱。

当原教旨主义者们指着圣经说，圣经里面明明白白写着上帝憎恨同性恋，你也翻出圣经指着创世纪说，那么不孕不育的夫妻是不是可以找个小三来代孕，他们必定立马说，这些经文是有历史背景的，不能从字面上来理解，按照圣经中的教导，家庭必须只有一男一女blahblahblah。

所以你看，原教旨主义者们并非不相信历史背景，原教旨主义者们也不是真正相信经典的一切都可按字面奉行。

经文是神圣的，不过某些经文，比另一些经文更神圣；经文都是上帝启示的，不过某些经文，比另一些经文是更重要的启示。而究竟哪些更神圣，哪些更重要，哪些是更需要暴力贯彻的启示，重点还是看话语权在谁的手中。

因此，在基列国的权力结构中，女性被剥夺阅读和识字的权力，以防她们能直接接触到真理而挑战权威解释；女性必须沉静恭顺，异见者除了手脚被拘束，口也被封住，生怕她们发出另类的声音。

所以说，所谓原教旨主义根本就是个伪命题，只是一个冠冕堂皇的掩饰，真正说话的，是躲在背后的权力结构。

**宗教并非女性的枷锁，父权主义才是**

于前现代的，父权的社会背景下写就的圣经，以及其他宗教经典，必然带有浓重的父权思想的沉渣，但即便高举圣经字句的原教旨主义者们，也不得不承认，随着社会思潮的发展和进步，随着对人权、平等、自由等现代思想的启蒙，没有任何宗教信仰可以完全地照搬经文来实践。

原教旨主义者或许会抓住圣经字句，反对同性恋、要求妻子顺服丈夫，但圣经中同样记载的，女人要蒙头，女人必须留长发，这些因为太落后于时代而连他们都得扬弃了。而至于女性是否可以讲道，担任圣职这样争议，虽然不同教派之间仍有争论，但比起写作年代的完全禁止，已经是进步了许多。

《使女的故事》写出的是一个以原教旨主义控制社会的集权政治模式，似乎让人觉得，就算宗教本身不是恶意的，但如果能被用来当成杀人工具，不如索性不要罢了。

但事实上，宗教并非没有主动性，只能容人随意装扮，宗教同样能够赋权于人，催人进步和解放。

正如十九世纪的基督新教，当它传入男权主义家长制的中国社会时，不仅没有被中国文化所同化，反而推动了中国女性地位提升和觉醒的第一步。基督教创办的育婴堂专收留被父母遗弃的女婴，将之养育长大并提供良好教育；不少教会学校是最早向男女开放的现代学校，更创办大量女子学校，补足私塾教育重男轻女的不足。

在女子学校中，学生们接受的除了宗教知识，更有现代科学知识和外语能力，使之谋得可以经济独立的能力，另一方面，西式学校注重体育、慈善等活动，促使女性更主动地参与社会与政治，成为变革中的中国中一股新的势力和声音。不少教会学校毕业的女性都表示，正是看到学校中来华传教或教书的独身女性，让她们渴望命运自主，并看到了相夫教子之外的其他想象，得以不被传统的家庭观念所禁锢，勇敢追寻自己的未来。

而尽管基督教会传统上一直扮演着反对同性恋的角色，但随着圣经诠释学和神学的发展，不少神学家和信徒开始反思，圣经中关于反同性恋的经文背后的真正涵义。

从历史处境和原文的角度重新解释，不仅为同性恋正名，更发展成了当代神学的显学：同志神学和酷儿神学，从性别认同被边缘化的群体的角度理解信仰。在同志平权运动之中，也有基督教会的身影，2013年香港同志友善教会就发起"彩虹之约"联署，呼吁教会接纳同志并支持争取平权运动。

在美国，不少教会不仅对同志的态度越来越正面和开放，甚至已经开始允许同志担任牧師，连传统大公教会圣公会在2003年都已经选出了第一位同志主教。这些例子正体现宗教并非腐朽不变，它同样可以与时俱进，甚至走在时代前列，实践对全人类和整个社会的博爱关怀。

无论是《使女的故事》中的基列国，还是当下宗教极端主义的阴云，都令人恐惧警惕。

但是，我们所要对抗的不是宗教本身，也不是某种宗教主义，而是躲在这些背后真正的敌人--根深蒂固的父权主义。无论是在宗教内，还是宗教外，发出女性自己的声音，发出挑战父权权力结构的声音，打破它的幽灵，而非皮相，才能自救。

本文作者系香港中文大学文化与宗教研究系博士候选人，生于上海，现居香港。研究领域：基督教历史、区域历史、民族认同与历史记忆**作者：卢梓淇**



![](http://pic.caixin.com/blog/Mon_1704/m_1492410037_MToaWw.jpg)

**《一念无明》剧照，主角在医院里，图片来源：豆瓣**



刚结束的金像奖颁奖礼上有两部现实主义题材电影，**关注了两个边缘群体：躁郁症患者和认知障碍患者。**关注躁郁症患者的是《一念无明》，主演余文乐，配角有曾志伟、金燕玲等。或许也是因为获奖效应吧，这部片子在大陆的排片量和去年的《幸运是我》相比要好得多了。



**刚好，我也是一个患有躁郁症的人，直到现在都不能说已经痊愈了。**每天我都不得不和形形色色的人打交道，当TA们知道我的这一情况后，会有各种回应：

**有的人是希望能帮助我，有的人是希望我能少为他们添麻烦，有的人则用所谓的“正常人”的标准来要求着我。**



无论是哪一类人，都有一些举动让我感到不舒服，对我造成冒犯，让我感受到不必要的压力，甚至恶化我的病情。**这篇文章，我想来谈谈，在我这个躁郁症患者的眼中，什么样的举动是让我感到舒服的。**

**帮助的前提是尊重与理解**



那些想要帮助我的人，往往对我的伤害是最深的。



TA们在并没有真正了解我的情况，**擅自给出TA们认为可行的建议，并且希望我能在听了建议后立刻付诸行动，切切实实地“改善”病情。**我的很多同学就属于这一类人。TA们给出的建议往往五花八门，有看韩剧，有放纵吃喝。



印象特别深的一个例子，是我的现任班长。可能是出于班长所承担的关心班集体成员身体状况的责任吧，TA仔细地询问了我的情况。我看在TA貌似诚恳的份儿上，就具体说了些我面临的困难：**我会无缘无故地失眠、食欲下降，而且对周遭事物提不起兴趣。**



![](http://pic.caixin.com/blog/Mon_1704/m_1492410048_yzOhRf.png)



TA给出的建议特别有意思，TA认为我面对的困难太多，不要想着一下子全部解决，而应该要一件事情一件事情地解决。**比如，我食欲莫名下降，我就只吃自己喜欢吃的东西；我睡眠不好，我就早点入睡，放空思绪。**



我还没来得及询问TA事先是否了解过躁郁症到底是怎么一回事，TA又接着说我需要多运动，多晒太阳。尤其是多晒太阳，这是TA的妈妈告诉TA的妙宗。在这场谈话中，**我的态度从愿意交流，慢慢地就转变成防御、抵抗，甚至不耐烦，只想着快点结束。哪怕TA给的建议是真正有效的，我也不会再听得进去了。**



![](http://pic.caixin.com/blog/Mon_1704/m_1492410055_YbGdiI.jpg)



与之相反的，是我的一个辅修应用心理学的同学。TA给的建议其实和那位班长所说的“多运动、多晒太阳”别无二致，但很重要的一点是，她在给了建议之后，补充了一点：**“其实也没什么，不是想给你什么建议之类的，就是想关心一下你。如果觉得没什么用可以不用管哈哈~”**



有了那位班长的经历，再加上那会儿我心情特别不好，这段谈话开始之前我已经有了明显的防御姿态了。**但TA这句话说出来之后，我反而舒畅了许多。**



TA并没有想要硬塞给我什么东西，与我始终保持着一种平等的姿态来对话。**至少，TA明确地给了我选择不采纳其建议的权利， TA甚至表明了自己不是来给建议的。**这种对话的方式，一下子就让我舒服了很多，哪怕我还没有采纳TA的“建议”，都能明显感觉到自己病情的改善。



当你想要帮助躁郁症患者时，最好是先了解躁郁症到底是怎么一回事。而且，释放善意在别人那儿或许有用，但在躁郁症患者身上，如果不和平等尊重的姿态相结合，那往往就事与愿违了。

**袖手旁观，就选择了墙**



希望我能少为TA们添麻烦的人当中，最典型的人恐怕就是我爸爸了。



我出自单亲家庭，成年之前我的抚养权归我妈。但他俩离婚之后，也不能说是老死不相往来，很多时候出于孩子的联结，还是有不少来往。我奶奶则特别关心我，认为我是家族里难得培养出来的一个文化人，她对我算得上是寄予厚望。



我第一次发病是2014年的时候，那会儿奶奶十分关心我的情况，屡屡来我家探望。**而我爸作为其儿子，貌似应该履行接送母亲往返的义务，再加上已经来到我家楼下，所以也会“形式地”上来我家问候两句。**那年我的病情好转得比较快，奶奶需要来我家探视的次数也算不上多，因而他也就基本都跟着来了。



![](http://pic.caixin.com/blog/Mon_1704/m_1492410065_rQpIHv.jpg)

**关于抑郁症的漫画，图片来源：网络**



**到了2016年11月，我第二次发病，而且比上一次持续时间长，病情更严重，我却再也没有看见他的身影。**奶奶倒是常来，不过听她每次来的口吻，她并不是每一次都能有人接送，有时候是自己坐公交车来我家和我说几句话的。偶尔是我爸接送，**他也没有上来我家坐坐，问问我的情况，稍稍关心几句。**



我想，其实早在2014年的时候，他就已经希望我能少为他添麻烦了吧？**莫名其妙的情绪病拖着总不见好，自己老妈老是要看望和自己已经没多大关系的孙子，害得自己老是要接送。**



当然了，我也没多把他当回事儿，**只是看着奶奶如此，我不得不埋怨自己让老人家受累，却也无能为力。**这种无能为力，既是没有办法想让病情好转就好转，也是没有办法安排奶奶的往返接送，更是没有办法抵抗原生家庭对自己的深刻影响，包括对病情的影响。



![](http://pic.caixin.com/blog/Mon_1704/m_1492410074_myheBG.png)

**“抑郁症者‘出柜’”漫画，图片来源：BUZZFEED**



有一个经典的说法，在墙和鸡蛋之间，如果你选择了袖手旁观，你就已经选择了墙。其实在情感障碍这件事情上，很多时候也是如此。**当你看到你身边的人因情感障碍身陷囹圄，自己却事不关己高高挂起，不要以为你这种行为对方看不到，也不要以为这对TA不会产生任何影响。**联系总是普遍存在的。

**世俗的标准，不都适用于TA们**



**更多的人是用所谓的“正常人”的标准来要求着我，这自然让我十分苦恼。**



躁郁症的特点是躁狂与抑郁并存，而我的情况是轻度的躁狂再加上明显的抑郁。明显的抑郁是很容易被感知、发现并诊断出来的，**但轻度的躁狂更多时候会被患者和周边的人理解为“正常”状态甚至是“兴奋”状态。**



这就很容易造成人们对躁郁症患者的误解。在我的大学里，老师在评估学生们平时成绩时很喜欢采用课堂展示的方式，也就是所谓的Presentation，简称Pre。



一个Pre往往会有一到两个主讲人，小组分工的时候再怎么进行严密、细致的分工，最后这个Pre的呈现效果好不好，责任都在主讲人身上。所以，在小组分工的时候，大家都对主讲人唯恐避之不及。



上学期一节电影艺术赏析课，同样需要小组展示。轮到我们那组的那一周，我之前几周一直病着，好不容易撑着去上几节课。在某节课上，我实在是撑不住了，就在课上睡着了。**但当我隐约听到课堂上谈论着我所感兴趣的性/别议题时，我立刻精神抖擞，举手向老师示意发言，在课堂上运用我所读到的性/别理论阐述情景案例，可以说是口若悬河了。**



然而正是我的这一举动，让我的组员们不相信我不能够在几天后的电影课上做Pre的主讲人。**TA们对我进行了各种隐/显性攻击，“你能够在别的课上口若悬河，在小组作业上就推卸责任，你难道不是只为个人利益着想吗？”；“你能够这么口若悬河，你好意思说你抑郁？你只是装出来的吧？”**诸如此类的言论最终将我逼上梁山，而组员中的某些人，最后也因此与我交恶。



![](http://pic.caixin.com/blog/Mon_1704/m_1492410084_VazfoT.jpg)

**关于抑郁症的漫画，图片来源：网络**



与之相反的，则是我这学期另外一个小组里的成员。这次的小组作业是要拍一个视频，我原本是要负责后期剪辑。但组员分工敲定不久，我的病情就恶化了，超过两个星期没有回学校上课，也没有和组员们联系。



后来我的情况稍有好转后，我陆续得知TA们已经把视频拍好了，甚至已经规划好要怎么剪辑，原本应该由我承担的剪辑工作已经分配给其他组员了。**我挺不好意思的，虽说我看上去是有着正当理由而不参加小组合作，但到底我还是拖累了别人，这也会给我造成心理压力。**



组长和我沟通时提出了一个意想不到的方案，竟完美地解决了这一事情下的诸多矛盾：TA很体谅我的状况，因此免除了我在这一次小组作业中所需要承担的工作。而有另一个小组作业，是其他组员不太感兴趣，而我很感兴趣，同时又是在大约两个月之后才需要上交成果的，TA希望能由我来负责。



TA的这种做法一来让现在的我能够好好休养，二来能够让我承担起应当的义务，三来能让我发挥专长，四来考虑到我的特殊情况所能承担的特殊任务，实在是一石四鸟。和上学期的那个小组相比，这位组长与躁郁症患者的相处才是正确的打开方式。



![](http://pic.caixin.com/blog/Mon_1704/m_1492410093_cZsnDi.jpg)

**关于抑郁症的漫画，图片来源：网络**



**世俗的“负责任”、“有始有终”、“一诺千金”的标准，在我身上并不能一直适用。**这并不是说我是一个不负责任的人，而是我的病情导致了我很多时候都不知道自己为什么会漠视TA人感受，不愿意与TA人联系，忽然人间蒸发。



我这么做，不仅仅会对TA人造成困扰，也会对我自身产生压力。所以可以的话，**请不要用世俗的、“正常人”的标准来要求我，那对我来说有时候是苛刻的。**

**最亲近的人，也可以是致命毒药**



最后还有一个不得不说的人，那就是我妈。



关于我妈、我和我的躁郁症，实在有太多、太多的故事可以分享和反思了。我就选其中的几个特别经典的，来让大家看看，**是不是躁郁症患者身边最亲近的人，就一定是TA们避风港。**



“精神病”在华语语境里很多时候和“神经病”没什么两样。在我的家乡，我出生之前就已经有了一所精神病专科医院。这所医院被建在了远离市区的地方，和癌症专科医院相靠。那时候人们认为癌症能够传染，癌症的死亡率又很高，所以就把癌症专科医院建在了如此偏僻的地方。**这所精神病专科医院在人们心目中的形象，可想而知。**



我妈在那个年代长大，受那些观念影响，认为那所精神病专科医院里的病人都是疯癫无状，流着口水，浑身脏兮兮的，衣衫不整，活脱脱一个发了疯的祥林嫂。



**正因如此，她十分拒绝将我视作一个“病人”，尤其是患有精神障碍的病人。**在她的眼里，一个考上了大学，品行优秀的儿子，怎么可能是发疯的祥林嫂呢？包括看病的时候也是如此。



她从来不带我去那所医院看病，只带我去家乡另一间三甲医院里的“心理咨询门诊”看病，因为这会让她觉得好受一些。这倒是其次，关键一点是，**每逢我在大街上和她谈及我的病情、用药等等涉及到我这一精神障碍的话题时，她总是要求我立马住嘴。**她不希望让别人知道我有这么个病，因为她觉得“别人不理解”。



另一件事情对我影响就更深了。她自己其实也有过抑郁症病史，但她只吃了三个月左右的药，后来停药后慢慢地就痊愈了。因此她觉得药物其实对病情恢复没多大作用。



![](http://pic.caixin.com/blog/Mon_1704/m_1492410103_CcyOQR.jpg)

**关于抑郁症的漫画，图片来源：网络**



我2014年第一次发病那会儿，看了医生之后，医生开了一个星期的药。我吃了两天，我妈看我吃了药后虽然睡眠有了大幅的改善，但目光呆滞，**觉得这些药效力太强了，再加上她本来就不相信药物的作用，就决定停药，不再复诊了。**当时正处于病情严重的状况，而且没有任何经济来源的我，是没办法改变她的这一决定的。虽然后来我用了差不多半年的时间，算是缓过来了，**但16年11月的复发，很可能就是因为14年那会儿我的病情并没有得到妥善的处置造成的。**



可以这么说，这半年我和我妈曾因为很多类似的事情吵过无数次，而很明显吵架也是于病情无益的。**有时候我甚至在想，我到底是在和这个病本身作斗争，还是和我妈的一些自作主张、先入为主的观念作斗争？我妈可以说是十分爱我的，但她真的帮到了我了吗？**



![](http://pic.caixin.com/blog/Mon_1704/m_1492410113_TBogtZ.jpg)

**关于抑郁症的漫画，图片来源：网络**



其实和躁郁症患者相处，与和其他别的人相处没什么很大的不同。**如果说真有不同的地方，那就是躁郁症患者自身的特殊情况，注定了TA们更容易被误解。**



其实人与人之间何尝不是充斥着大量的误解呢？**消除误解最简单的方法，就是静下心来，走进对方的世界，用别人的眼睛来观察这个曾经熟悉的世界。**
