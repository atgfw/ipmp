---
templateKey: article
title: 生鲜超市“浪费食物”并非无解，以香港“惜食堂”为例
slug: hong-kong-stop-waste-food
date: 2019-09-03T05:46:45.333Z
featuredpost: false
featuredimage: /img/food.webp
trending: 1
isbrief: false
tags:
  - 公益
  - 社会热点
---
作者丨捞面

编辑丨阿八

昨日，微博用户“小椅子Zoe”发文，感慨某生鲜超市扔掉当天临期食物，引起了共鸣与热议。一边有网友说，这是“巨大的浪费”，还引出了麦当劳、肯德基等餐饮企业都有类似规定以作感慨；另一边呢，则是认为超市此举实属正常，只是企业不愿承担风险的商业决策。观点当然不止以上两种，这里也不一一列举。

在诸多观点中，有一种观点是这样的：对于临期食物的“拯救”，责任不在于企业，政府和社会组织应当承担更多的责任。

食物银行（FoodBank）即是践行上述观点的其中一种解决方法。顾名思义，食物银行即组织或个人将仍可安全食用的临期食品捐献给慈善团体，再由这些团体经过处理后免费提供给需要食物的人。世界上第一所食物银行出现在1967年的亚利桑那州凤凰城，此模式亦在此后数十年间在各国各地发展，至今规模已达数千家。

在国内，上海绿洲公益发展中心亦在2014年推出了首家食物银行，不过，由于国内食品安全相关法规禁止过期食品流通，而非如一些国家与地区使用”最佳赏味期“这一做法等原因，食物银行在国内的发展并不顺利。

本文会介绍在香港颇为有名的食物回收利用组织“惜食堂”，给读者提供一个观察的角度。

## 惜食堂（Food Angel）


惜食堂是由小宝慈善基金2011年开始的一个项目，简单点来说，他们会向本地的餐饮企业及个人回收剩余食物及食材，经过处理后再制作成热饭餐或食物包，随后再通过其物流团队送至有需求的人群。

![](https://img9.doubanio.com/view/note/l/public/p64856893.webp)正在处理食物的义工，图源：惜食堂官网


根据官网的介绍，惜食堂如今从超过两百个餐馆处回收食材，还在15个商场内设置了食品收集箱，目前平均每星期回收食材可达到30吨。这些回收得到的食材将送至一个名为“惜食分饷站”的菜蔬处理中心。中心会对瓜果蔬菜、干货等进行清洗、分类等工作，再送至一个名为“丰膳坊”的中央食物制造厂及两个本地厨房。惜食堂统计指出，目前每日可制作8000份热饭餐及1000份食物包。这些饭食最终会通过惜食堂的物流团队送出。

![](https://img9.doubanio.com/view/note/l/public/p64856901.webp)惜食堂的物流团队目前拥有6辆附有冷冻设备的货车及25辆轻型货车，图源：惜食堂官网


惜食堂2017年4月至2018年3月的年报显示，这一年惜食堂共回收了超过1700吨的食物，其中包括16898个面包。向惜食堂捐赠剩余食物的机构则达到了284家。

这些剩余食物最终变成了超过215万盒的热饭餐和超过53万个食物包，其中95%被送至合作的慈善机构及社区中心、老人之家等场所。据该年报统计，受惠于这些食物的人有73%为长者，16%为低收入家庭和儿童。

以惜食堂自建的社区中心为例。该中心位于深水埗区，根据2018年政府统计处的数据，深水埗区的家庭月收入中位数在全港18个区中排名倒数第二，为23100元，与排在第一的湾仔区相差近一倍；区内存在不少贫困长者与低收入家庭。

演员黄子华曾为惜食堂拍过一段宣传片（详情请点击“惜食堂”），其中讲到了社区中心的运作。目前，惜食堂社区中心每天将两次开放，为来者提供热饭餐。其中一次为下午四点半开始，提供给长者就餐；一次为下午五点四十五分，提供给低收入家庭就餐。惜食堂统计，现在每天约有300位长者和两百人次的家庭到社区中心就餐，另外还有约200位独居或行动不便的长者，则有专门的外送人员送餐上门。

![](https://img9.doubanio.com/view/note/l/public/p64856915.webp)外送团队，图源：惜食堂官网


黄子华所参与拍摄的短片中，曾女士一家四口人，月收入仅有1.3万港币，符合惜食堂定下的家庭月收入一万七千六百的标准，因此晚上会和两个孩子一起到中心吃饭，她的丈夫有时还会直接在中心和他们碰面。按照港府定下的贫穷线标准——四口之家月收入一万九千九百以下，曾女士一家人即属于贫穷家庭。一家人目前住在房租四千元、地方狭小的㓥房之中，哪怕按照四口人一餐五十元的标准来算的话，食物回收的好处是为一家人省下了足足一千五百元。

![](https://img9.doubanio.com/view/note/l/public/p64856931.webp)曾女士女儿正在吃饭，图为视频截图


惜食堂也并非一开始就有如此大的规模。惜食堂第一年回收到的剩余食物只有二十吨，还不到现在一周的回收量。如前述所说的几个处理中心，柴湾的厨房于2012年7月投入运行，深水埗的厨房于2013年12月投入运行。至于菜蔬处理中心和中央食物制造厂则更是分别在2016年和2019年才投入服务。

当然，总体而言，惜食堂的发展还算一帆风顺。从获得资助一项亦可看出。根据惜食堂2017到2018的年报，惜食堂这一年度的总支出为35446193港元。年度捐赠收入则是55908027港元。

另外，不少富商、明星亦有对惜食堂表示支持，其中包括李嘉诚、张学友、薛家燕等。甚至，连特首林郑亦曾到访过惜食堂。

惜食堂目前的团队成员约为一百五十人左右，其中数量最多的是运营人员，约为一百人。在核心团队成员以外，义工亦成为项目做到现在不可或缺的部分。惜食堂统计显示，几个驻点每日共需要约两百名义工提供服务，至今已有超过4.6万人次的义工参与到项目中。这些义工主要参与瓜蔬的处理、饭餐的预备与派发。

在香港，惜食堂也并不是唯一一家”食物银行“。谷歌搜索“食物回收”，置顶的几个广告都是香港本地的食物回收再分享的组织。香港地球之友的“救食平台·食物回收捐助联盟”中，有五十九家机构均可接收剩余食物。